#!/bin/bash
cd ../WebApplication1
docker login -u $DOCKER_USERNAME -p $DOCKER_PASSWORD
docker build -t webapplication1 .
docker tag webapplication1 $DOCKER_USERNAME/webapplication1
docker push $DOCKER_USERNAME/webapplication1
